geotab.addin.hello = (elt, service) => {
    // TODO: make a multicall to geotab api
    /*api.multiCall([['Get', {
             "typeName":"Device",
               "search":{
                  "id":"b2"
               }
         }],
         ['Get', {
             "typeName":"StatusData",
               "search":{
                  "deviceSearch":{
                     "id":"b2"
                  },
                  "diagnosticSearch":{
                     "id":"DiagnosticEngineHoursAdjustmentId"
                  },
                  "fromDate":searchDate,
                  "toDate":searchDate
               }
         }],
         ['Get', {
               "typeName":"StatusData",
               "search":{
                  "deviceSearch":{
                     "id":"b2"
                  },
                  "diagnosticSearch":{
                     "id":"DiagnosticOdometerAdjustmentId"
                  },
                  "fromDate":"2019-07-11T09:02:51.528Z",
                  "toDate":"2019-07-11T09:02:51.528Z"
               }
            }],
            ['Get', {
               "typeName":"TripTypeChange",
               "search":{
                  "deviceSearch":{
                     "id":"b2"
                  },
                  "toDate":"2019-07-11T09:02:51.528Z",
                  "fromDate":"2019-07-11T09:02:51.528Z",
                  "includeFromDateOverlap":true
               }
            }],
            ['Get', {
               "typeName":"DriverChange",
               "search":{
                  "deviceSearch":{
                     "id":"b2"
                  },
                  "type":"Driver",
                  "fromDate":"2019-07-11T09:02:51.528Z",
                  "toDate":"2019-07-11T09:02:51.528Z",
                  "includeOverlappedChanges":true
               }
            }],
            ['Get', {
               "typeName":"WorkTime"
            }],
            ['GetTimeZones', {

            }],
            ['Get', {
               "typeName":"SystemSettings"
            }]], function(results) {
        console.log(JSON.stringify(results));
    }, console.error);*/

    // Response to multicall
    const results = [
        [
            {
                "auxWarningSpeed": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                ],
                "enableAuxWarning": [
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false
                ],
                "enableControlExternalRelay": false,
                "externalDeviceShutDownDelay": 0,
                "immobilizeArming": 30,
                "immobilizeUnit": false,
                "isAuxIgnTrigger": [
                    false,
                    false,
                    false,
                    false
                ],
                "isAuxInverted": [
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false
                ],
                "accelerationWarningThreshold": 24,
                "accelerometerThresholdWarningFactor": 0,
                "brakingWarningThreshold": -34,
                "corneringWarningThreshold": 26,
                "enableBeepOnDangerousDriving": false,
                "enableBeepOnRpm": false,
                "engineHourOffset": 0,
                "isActiveTrackingEnabled": true,
                "isDriverSeatbeltWarningOn": false,
                "isPassengerSeatbeltWarningOn": false,
                "isReverseDetectOn": false,
                "odometerFactor": 1,
                "odometerOffset": 0,
                "rpmValue": 3500,
                "seatbeltWarningSpeed": 10,
                "activeFrom": "2019-05-30T19:07:56.500Z",
                "activeTo": "2050-01-01",
                "autoGroups": [],
                "customParameters": [],
                "disableBuzzer": false,
                "enableBeepOnIdle": false,
                "enableMustReprogram": false,
                "enableSpeedWarning": false,
                "engineType": "EngineTypeGenericId",
                "engineVehicleIdentificationNumber": "@@@@@@@@@@@@@@@@@",
                "ensureHotStart": false,
                "gpsOffDelay": 240,
                "idleMinutes": 3,
                "isSpeedIndicator": false,
                "licensePlate": "102",
                "licenseState": "DUBAI",
                "major": 18,
                "minAccidentSpeed": 4,
                "minor": 53,
                "parameterVersion": 6,
                "pinDevice": true,
                "speedingOff": 90,
                "speedingOn": 100,
                "vehicleIdentificationNumber": "",
                "goTalkLanguage": "English",
                "fuelTankCapacity": 0,
                "comment": "COBUS 102",
                "groups": [
                    {
                        "children": [],
                        "id": "GroupCompanyId"
                    }
                ],
                "timeZoneId": "America/New_York",
                "deviceFlags": {
                    "activeFeatures": [
                        "GoActive",
                        "UnknownDevice"
                    ],
                    "isActiveTrackingAllowed": true,
                    "isEngineAllowed": true,
                    "isGarminAllowed": true,
                    "isHOSAllowed": true,
                    "isIridiumAllowed": true,
                    "isOdometerAllowed": true,
                    "isTripDetailAllowed": true,
                    "isUIAllowed": true,
                    "isVINAllowed": true,
                    "ratePlans": []
                },
                "deviceType": "GO7",
                "hardwareId": 553591409,
                "id": "b2",
                "ignoreDownloadsUntil": "1986-01-01",
                "maxSecondsBetweenLogs": 200,
                "name": "102",
                "productId": 113,
                "serialNumber": "G77020FF2271",
                "timeToDownload": "1.00:00:00",
                "workTime": "WorkTimeStandardHoursId",
                "devicePlans": [
                    "ProPlus"
                ]
            }
        ],
        [
            {
                "data": 1267312.7739999997,
                "dateTime": "2019-07-11T09:02:51.528Z",
                "device": {
                    "id": "b2"
                },
                "diagnostic": {
                    "id": "DiagnosticEngineHoursAdjustmentId"
                },
                "controller": "ControllerNoneId"
            }
        ],
        [
            {
                "data": 1919825.2748907804,
                "dateTime": "2019-07-11T09:02:51.528Z",
                "device": {
                    "id": "b2"
                },
                "diagnostic": {
                    "id": "DiagnosticOdometerAdjustmentId"
                },
                "controller": "ControllerNoneId"
            }
        ],
        [],
        [],
        [
            {
                "holidayGroup": {
                    "groupId": 1
                },
                "id": "WorkTimeStandardHoursId",
                "details": [
                    {
                        "dayOfWeek": "Monday",
                        "fromTime": "08:00:00",
                        "toTime": "17:00:00",
                        "id": "b1"
                    },
                    {
                        "dayOfWeek": "Tuesday",
                        "fromTime": "08:00:00",
                        "toTime": "17:00:00",
                        "id": "b2"
                    },
                    {
                        "dayOfWeek": "Wednesday",
                        "fromTime": "08:00:00",
                        "toTime": "17:00:00",
                        "id": "b3"
                    },
                    {
                        "dayOfWeek": "Thursday",
                        "fromTime": "08:00:00",
                        "toTime": "17:00:00",
                        "id": "b4"
                    },
                    {
                        "dayOfWeek": "Friday",
                        "fromTime": "08:00:00",
                        "toTime": "17:00:00",
                        "id": "b5"
                    }
                ]
            },
            "WorkTimeAllHoursId",
            {
                "holidayGroup": {
                    "groupId": 1
                },
                "id": "WorkTimeLunchHoursId",
                "details": [
                    {
                        "dayOfWeek": "Monday",
                        "fromTime": "11:00:00",
                        "toTime": "14:00:00",
                        "id": "b17"
                    },
                    {
                        "dayOfWeek": "Tuesday",
                        "fromTime": "11:00:00",
                        "toTime": "14:00:00",
                        "id": "b18"
                    },
                    {
                        "dayOfWeek": "Wednesday",
                        "fromTime": "11:00:00",
                        "toTime": "14:00:00",
                        "id": "b19"
                    },
                    {
                        "dayOfWeek": "Thursday",
                        "fromTime": "11:00:00",
                        "toTime": "14:00:00",
                        "id": "b1A"
                    },
                    {
                        "dayOfWeek": "Friday",
                        "fromTime": "11:00:00",
                        "toTime": "14:00:00",
                        "id": "b1B"
                    }
                ]
            },
            {
                "holidayGroup": {
                    "groupId": 1
                },
                "id": "WorkTimeEarlyLeaveHoursId",
                "details": [
                    {
                        "dayOfWeek": "Monday",
                        "fromTime": "16:00:00",
                        "toTime": "16:45:00",
                        "id": "b12"
                    },
                    {
                        "dayOfWeek": "Tuesday",
                        "fromTime": "16:00:00",
                        "toTime": "16:45:00",
                        "id": "b13"
                    },
                    {
                        "dayOfWeek": "Wednesday",
                        "fromTime": "16:00:00",
                        "toTime": "16:45:00",
                        "id": "b14"
                    },
                    {
                        "dayOfWeek": "Thursday",
                        "fromTime": "16:00:00",
                        "toTime": "16:45:00",
                        "id": "b15"
                    },
                    {
                        "dayOfWeek": "Friday",
                        "fromTime": "16:00:00",
                        "toTime": "16:45:00",
                        "id": "b16"
                    }
                ]
            },
            {
                "holidayGroup": {
                    "groupId": 1
                },
                "id": "WorkTimeLateArrivalHoursId",
                "details": [
                    {
                        "dayOfWeek": "Monday",
                        "fromTime": "09:30:00",
                        "toTime": "10:30:00",
                        "id": "bD"
                    },
                    {
                        "dayOfWeek": "Tuesday",
                        "fromTime": "09:30:00",
                        "toTime": "10:30:00",
                        "id": "bE"
                    },
                    {
                        "dayOfWeek": "Wednesday",
                        "fromTime": "09:30:00",
                        "toTime": "10:30:00",
                        "id": "bF"
                    },
                    {
                        "dayOfWeek": "Thursday",
                        "fromTime": "09:30:00",
                        "toTime": "10:30:00",
                        "id": "b10"
                    },
                    {
                        "dayOfWeek": "Friday",
                        "fromTime": "09:30:00",
                        "toTime": "10:30:00",
                        "id": "b11"
                    }
                ]
            }
        ],
        [
            {
                "id": "Antarctica/Casey",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "America/Buenos_Aires",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Europe/Copenhagen",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Europe/Busingen",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Guayaquil",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Etc/GMT-13",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "13:00:00"
            },
            {
                "id": "Pacific/Saipan",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Africa/Maputo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "America/Havana",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Asia/Tokyo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "America/Puerto_Rico",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Africa/Tripoli",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Dili",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "America/Anchorage",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-09:00:00"
            },
            {
                "id": "America/Cayenne",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Africa/Libreville",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Khandyga",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "America/Guatemala",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Asia/Pontianak",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Africa/Lagos",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Nicosia",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "MST7MDT",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Europe/Kirov",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Africa/Bamako",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/Indianapolis",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Asia/Tomsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Indian/Mauritius",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Pacific/Tongatapu",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "13:00:00"
            },
            {
                "id": "Asia/Almaty",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "Asia/Novosibirsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Indian/Antananarivo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Indian/Mahe",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Asia/Kuala_Lumpur",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Antarctica/Davis",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Australia/Sydney",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Europe/Moscow",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/Indiana/Vincennes",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Europe/Belgrade",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Santo_Domingo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Curacao",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Marigot",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Sao_Paulo",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Asia/Amman",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Atlantic/Faeroe",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Europe/Samara",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "America/Tijuana",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-08:00:00"
            },
            {
                "id": "Europe/Isle_of_Man",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Atlantic/Canary",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Europe/Podgorica",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Indiana/Marengo",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Pacific/Galapagos",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Maceio",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Atlantic/Bermuda",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Australia/Brisbane",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "America/Costa_Rica",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Europe/Gibraltar",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Juneau",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-09:00:00"
            },
            {
                "id": "Australia/Adelaide",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "09:30:00"
            },
            {
                "id": "Pacific/Honolulu",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-10:00:00"
            },
            {
                "id": "Atlantic/Cape_Verde",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-01:00:00"
            },
            {
                "id": "Asia/Dubai",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Indian/Mayotte",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "CST6CDT",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Africa/Dakar",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/St_Vincent",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Pacific/Nauru",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "Asia/Thimphu",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "America/Eirunepe",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Pacific/Kosrae",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "America/Swift_Current",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Mendoza",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Europe/Oslo",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Port-au-Prince",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "America/Bahia_Banderas",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Africa/Harare",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "America/Metlakatla",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-09:00:00"
            },
            {
                "id": "Asia/Damascus",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Etc/UTC",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Europe/Stockholm",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Australia/Melbourne",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Africa/El_Aaiun",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Dawson_Creek",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Asia/Yakutsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "America/Argentina/Rio_Gallegos",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Asia/Muscat",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Europe/Prague",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Makassar",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "America/Lima",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "America/Santa_Isabel",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-08:00:00"
            },
            {
                "id": "America/North_Dakota/Center",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Boa_Vista",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Indian/Reunion",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Asia/Omsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Asia/Anadyr",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "America/Iqaluit",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Europe/Istanbul",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Europe/Minsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Indian/Chagos",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "Antarctica/Mawson",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "Pacific/Bougainville",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Europe/Saratov",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Antarctica/Syowa",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Etc/GMT-9",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "America/Pangnirtung",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Asia/Ust-Nera",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Asia/Seoul",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "America/St_Kitts",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Fort_Nelson",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Asia/Dushanbe",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "America/Argentina/San_Juan",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Pacific/Apia",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "13:00:00"
            },
            {
                "id": "Europe/Tirane",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Africa/Lome",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Europe/Jersey",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/Menominee",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Glace_Bay",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Etc/GMT+11",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-11:00:00"
            },
            {
                "id": "America/Managua",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Pacific/Niue",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-11:00:00"
            },
            {
                "id": "America/Nome",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-09:00:00"
            },
            {
                "id": "America/Nipigon",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "America/Catamarca",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Etc/GMT-14",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "14:00:00"
            },
            {
                "id": "America/Boise",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Europe/Rome",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Africa/Bangui",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Africa/Tunis",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Africa/Lubumbashi",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Africa/Kampala",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Africa/Freetown",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Etc/GMT-3",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/Louisville",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Asia/Jerusalem",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Baku",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Etc/GMT+10",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-10:00:00"
            },
            {
                "id": "Africa/Asmera",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/North_Dakota/Beulah",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Detroit",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Europe/Paris",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Manaus",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Asia/Sakhalin",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "America/Barbados",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Asia/Hong_Kong",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Etc/GMT+3",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Pacific/Tahiti",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-10:00:00"
            },
            {
                "id": "America/Recife",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Africa/Ndjamena",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Europe/Chisinau",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Pacific/Fiji",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "Africa/Mbabane",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Pacific/Palau",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "America/Creston",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Europe/Madrid",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Australia/Hobart",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "America/Panama",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Europe/Monaco",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Etc/GMT",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/St_Johns",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:30:00"
            },
            {
                "id": "Antarctica/Macquarie",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "America/Tortola",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Asia/Chita",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "Etc/GMT-8",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Africa/Nouakchott",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/Rio_Branco",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Antarctica/South_Pole",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "America/El_Salvador",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Asia/Vladivostok",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Europe/Athens",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Etc/GMT-7",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Asia/Katmandu",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:45:00"
            },
            {
                "id": "America/Tegucigalpa",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Regina",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Asia/Kashgar",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Asia/Yerevan",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Asia/Harbin",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "America/Araguaina",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "America/North_Dakota/New_Salem",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Winnipeg",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Argentina/San_Luis",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "America/New_York",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Africa/Juba",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/Sitka",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-09:00:00"
            },
            {
                "id": "Europe/Vatican",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Europe/Tallinn",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Pacific/Noumea",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Australia/Darwin",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "09:30:00"
            },
            {
                "id": "America/Indiana/Vevay",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Africa/Blantyre",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Africa/Ceuta",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Krasnoyarsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Australia/Broken_Hill",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "09:30:00"
            },
            {
                "id": "America/Thunder_Bay",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Etc/GMT-10",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Europe/Luxembourg",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Cancun",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Asia/Aqtobe",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "America/Goose_Bay",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Asia/Colombo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:30:00"
            },
            {
                "id": "America/Caracas",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Etc/GMT-12",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "Europe/Mariehamn",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "America/Campo_Grande",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Europe/Vaduz",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Africa/Algiers",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Godthab",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Asia/Qatar",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/Montreal",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "America/Shiprock",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Atlantic/Madeira",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Asia/Singapore",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "America/Phoenix",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "America/Grenada",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Asia/Choibalsan",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Pacific/Wake",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "America/Merida",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Asia/Baghdad",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Europe/Zurich",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "PST8PDT",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-08:00:00"
            },
            {
                "id": "Antarctica/Palmer",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Indiana/Winamac",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "America/Blanc-Sablon",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Pacific/Truk",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Africa/Sao_Tome",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Africa/Mogadishu",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/Bogota",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Asia/Qyzylorda",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "Atlantic/South_Georgia",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-02:00:00"
            },
            {
                "id": "America/Inuvik",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "America/Los_Angeles",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-08:00:00"
            },
            {
                "id": "Europe/Vienna",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Europe/Volgograd",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Indian/Cocos",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:30:00"
            },
            {
                "id": "Asia/Tashkent",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "Australia/Lindeman",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Africa/Niamey",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Beirut",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Etc/GMT+6",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Europe/Vilnius",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "America/Lower_Princes",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Indiana/Tell_City",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "EST5EDT",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Antarctica/Rothera",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Europe/Berlin",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Yellowknife",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "America/Danmarkshavn",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Etc/GMT+5",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "America/Kentucky/Monticello",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Europe/Amsterdam",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Port_of_Spain",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Asia/Hebron",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Vientiane",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "America/Whitehorse",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-08:00:00"
            },
            {
                "id": "Africa/Ouagadougou",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/Montserrat",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/St_Barthelemy",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Africa/Casablanca",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Australia/Currie",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "America/St_Thomas",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Etc/GMT+1",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-01:00:00"
            },
            {
                "id": "Indian/Maldives",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "America/Rankin_Inlet",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Etc/GMT+4",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Asia/Srednekolymsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Europe/Astrakhan",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Etc/GMT-2",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Dhaka",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "Pacific/Wallis",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "Pacific/Pago_Pago",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-11:00:00"
            },
            {
                "id": "Africa/Kigali",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Africa/Maseru",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Irkutsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Etc/GMT+12",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-12:00:00"
            },
            {
                "id": "Asia/Tbilisi",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "America/Moncton",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Dominica",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Kralendijk",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Chihuahua",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "America/Thule",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Chicago",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Asia/Kabul",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "04:30:00"
            },
            {
                "id": "Asia/Pyongyang",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "Europe/Brussels",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Coral_Harbour",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Europe/Zagreb",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Africa/Khartoum",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Asia/Aden",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/Asuncion",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Antarctica/McMurdo",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "America/Denver",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Asia/Novokuznetsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Pacific/Funafuti",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "America/Nassau",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Asia/Samarkand",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "Asia/Rangoon",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:30:00"
            },
            {
                "id": "Etc/GMT+7",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Europe/Guernsey",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Asia/Kamchatka",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "America/Belem",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Pacific/Easter",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Asia/Oral",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "America/Belize",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Anguilla",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Europe/Kaliningrad",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Manila",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Pacific/Port_Moresby",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Africa/Djibouti",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Asia/Chongqing",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Africa/Johannesburg",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Shanghai",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "America/Antigua",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Europe/Budapest",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Africa/Bujumbura",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Europe/Kiev",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Gaza",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Antarctica/Vostok",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "America/Monterrey",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Dawson",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-08:00:00"
            },
            {
                "id": "Antarctica/DumontDUrville",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "Africa/Banjul",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Africa/Dar_es_Salaam",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/Fortaleza",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "America/Indiana/Petersburg",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Europe/Andorra",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Indian/Kerguelen",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "Europe/London",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/Resolute",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "America/Jujuy",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "America/Paramaribo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Pacific/Kwajalein",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "America/Bahia",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "America/Martinique",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Africa/Cairo",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "America/Porto_Velho",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Africa/Nairobi",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Etc/GMT+2",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-02:00:00"
            },
            {
                "id": "Asia/Ulaanbaatar",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Pacific/Guam",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "10:00:00"
            },
            {
                "id": "America/Santarem",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Etc/GMT-1",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Cambridge_Bay",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "America/Grand_Turk",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Argentina/Salta",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Asia/Macau",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Pacific/Johnston",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-10:00:00"
            },
            {
                "id": "Asia/Hovd",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Asia/Magadan",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Asia/Bangkok",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "America/Aruba",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Pacific/Guadalcanal",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Europe/San_Marino",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Indian/Christmas",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Atlantic/St_Helena",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/Mazatlan",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "America/Montevideo",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Etc/GMT-11",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Pacific/Kiritimati",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "14:00:00"
            },
            {
                "id": "America/Guyana",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Cuiaba",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Asia/Riyadh",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Asia/Aqtau",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "America/Edmonton",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Asia/Bishkek",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "Asia/Urumqi",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "Pacific/Tarawa",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "Europe/Sarajevo",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Atlantic/Stanley",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Europe/Skopje",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Santiago",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Europe/Dublin",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/Scoresbysund",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-01:00:00"
            },
            {
                "id": "America/Cayman",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Africa/Accra",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Africa/Gaborone",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "America/Toronto",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "America/Indiana/Knox",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Pacific/Efate",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Pacific/Rarotonga",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-10:00:00"
            },
            {
                "id": "America/Ojinaga",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Pacific/Auckland",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "Pacific/Majuro",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "12:00:00"
            },
            {
                "id": "America/Mexico_City",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Etc/GMT-6",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "06:00:00"
            },
            {
                "id": "America/Noronha",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-02:00:00"
            },
            {
                "id": "Pacific/Norfolk",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Europe/Helsinki",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "America/Argentina/Ushuaia",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Europe/Sofia",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Calcutta",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:30:00"
            },
            {
                "id": "Etc/GMT-4",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Asia/Jayapura",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "09:00:00"
            },
            {
                "id": "America/Argentina/Tucuman",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Africa/Bissau",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Pacific/Fakaofo",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "13:00:00"
            },
            {
                "id": "America/Yakutat",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-09:00:00"
            },
            {
                "id": "Indian/Comoro",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Europe/Ulyanovsk",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "04:00:00"
            },
            {
                "id": "Asia/Karachi",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "Africa/Addis_Ababa",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "America/La_Paz",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Africa/Abidjan",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Africa/Kinshasa",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Europe/Simferopol",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Atlantic/Azores",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-01:00:00"
            },
            {
                "id": "Australia/Perth",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Arctic/Longyearbyen",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/Guadeloupe",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Europe/Bratislava",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Barnaul",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "America/Argentina/La_Rioja",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Pacific/Ponape",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "11:00:00"
            },
            {
                "id": "Europe/Ljubljana",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Europe/Malta",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Tehran",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "03:30:00"
            },
            {
                "id": "America/Halifax",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "America/Vancouver",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-08:00:00"
            },
            {
                "id": "Europe/Zaporozhye",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Jakarta",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Atlantic/Reykjavik",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Asia/Yekaterinburg",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "Asia/Kuching",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "America/Matamoros",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Europe/Uzhgorod",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Africa/Conakry",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Europe/Warsaw",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Kuwait",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Europe/Lisbon",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "America/Hermosillo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-07:00:00"
            },
            {
                "id": "Africa/Douala",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Bahrain",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "03:00:00"
            },
            {
                "id": "Africa/Porto-Novo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "America/St_Lucia",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-04:00:00"
            },
            {
                "id": "Pacific/Enderbury",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "13:00:00"
            },
            {
                "id": "America/Cordoba",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-03:00:00"
            },
            {
                "id": "Asia/Brunei",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "America/Jamaica",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-05:00:00"
            },
            {
                "id": "Africa/Windhoek",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Africa/Luanda",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Asia/Saigon",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "America/Rainy_River",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "-06:00:00"
            },
            {
                "id": "Africa/Lusaka",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Asia/Ashgabat",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "Africa/Brazzaville",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Africa/Monrovia",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "00:00:00"
            },
            {
                "id": "Europe/Riga",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            },
            {
                "id": "Africa/Malabo",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "01:00:00"
            },
            {
                "id": "Pacific/Midway",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "-11:00:00"
            },
            {
                "id": "Asia/Phnom_Penh",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "07:00:00"
            },
            {
                "id": "Etc/GMT-5",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "05:00:00"
            },
            {
                "id": "Asia/Taipei",
                "isDaylightSavingTimeSupported": false,
                "offsetFromUtc": "08:00:00"
            },
            {
                "id": "Europe/Bucharest",
                "isDaylightSavingTimeSupported": true,
                "offsetFromUtc": "02:00:00"
            }
        ],
        [
            {
                "emailSenderFrom": "NotificationSender@geotab.com",
                "allowUnsignedAddIn": true,
                "allowThirdPartyMarketplaceApps": true,
                "allowMarketplacePurchases": true,
                "companyAddress": "",
                "companyGuid": "ae64b24d-1b9d-4ec6-b3a7-64dd964c0ebe",
                "companyName": "Dnata Test",
                "customCode": "",
                "customerPages": [
                    "{\"url\":\"https://app.geotab.com/addins/zenduit/zendu-maintenance/manifest.json\"}",
                    "{\"name\":\"Zone Route List\",\"supportEmail\":\"support@zenduit.com\",\"version\":\"1.0\",\"items\":[{\"url\":\"https://apps.zenduit.com/drive-routes/assets/static/addin.html\",\"path\":\"DriveAppLink/\",\"icon\":\"https://apps.zenduit.com/drive-routes/assets/img/icon.png\",\"menuName\":{\"en\":\"Zone/Route List\"}}],\"isSigned\":false}",
                    "{\"name\":\"Hours Report\",\"supportEmail\":\"support@zenduit.com\",\"version\":\"1.0\",\"items\":[{\"url\":\"https://devsreport.zenduit.com/addin/assets/static/addin.html\",\"icon\":\"https://devsreport.zenduit.com/addin/assets/img/icon.png\",\"path\":\"DataEngineLink/\",\"menuName\":{\"en\":\"Hours Report\"}}],\"isSigned\":false}",
                    "{\"name\":\"Playback\",\"supportEmail\":\"support@zenduit.com\",\"version\":\"1.1\",\"items\":[{\"url\":\"https://apps.zenduit.com/RoutePlayback/playback.html\",\"path\":\"ZoneAndMessagesLink/\",\"icon\":\"https://apps.zenduit.com/RoutePlayback/images/icon2.png\",\"menuName\":{\"en\":\"ZenduIT Playback\"}}],\"isSigned\":false}",
                    "{\"name\":\"HOS Violation Alerts V2\",\"supportEmail\":\"support@zenduit.com\",\"version\":\"1.0\",\"items\":[{\"url\":\"https://hos.zenduit.com/addin/assets/static/addin.html\",\"path\":\"HosLink/\",\"icon\":\"https://hos.zenduit.com/addin/assets/img/warning.png\",\"menuName\":{\"en\":\"HOS Alerts\"}}],\"isSigned\":false}",
                    "{\"url\":\"https://app.geotab.com/addins/geotab/eld-settings-validator/manifest.json\"}",
                    "{\"url\":\"https://app.geotab.com/addins/geotab/roadside-assistance/manifest.json\"}",
                    "{\"url\":\"https://app.geotab.com/addins/geotab/eld-support/manifest.json\"}",
                    "{\"supportEmail\":\"support@example.com\",\"signature\":\"12345-MYSIGNATURE\",\"name\":\"Trips Street View (by Geotab)\",\"items\":[{\"icon\":\"https://cdn.rawgit.com/Geotab/addin-trips-streetview/master/images/icon.svg\",\"path\":\"ActivityLink/\",\"menuName\":{\"en\":\"Trips Street View\"},\"url\":\"https://cdn.rawgit.com/Geotab/addin-trips-streetview/master/tripsStreetView.html\"}],\"version\":\"1.0\",\"key\":\"12345-MYAPIKEY\",\"isSigned\":false}",
                    "{\"url\":\"https://app.geotab.com/addins/geotab/accidentreconstruction/manifest.json\"}",
                    "{\"name\":\"Public Map\",\"supportEmail\":\"support@zenduit.com\",\"version\":\"1.0.0\",\"items\":[{\"path\":\"\",\"menuName\":{\"en\":\"Public Map\",\"fr\":\"Public Map\"},\"url\":\"https://bobo.zenduit.com/AddIn/ZenduBOBO/Install?addin=publicmap&nongeotabbilling=true\",\"icon\":\"https://publicmap2.zenduit.com/AddIn/assets/icon.png\"}],\"isSigned\":false}",
                    "{\"name\":\"ZenduCAM\",\"supportEmail\":\"support@zenduit.com\",\"version\":\"1.0\",\"items\":[{\"url\":\"https://trax.zenduit.com/trax.html\",\"path\":\"ZenduCAM\",\"icon\":\"https://trax.zenduit.com/Images/ZenducamIcon.png\",\"menuName\":{\"en\":\"ZenduCAM\"}}],\"isSigned\":false}",
                    "{\"supportEmail\":\"support@zenduit.com\",\"signature\":\"12345-MYSIGNATURE\",\"name\":\"Zenduit Proximity\",\"items\":[{\"icon\":\"https://apps.zenduit.com/proximity/images/icon.png\",\"path\":\"ZoneAndMessagesLink/\",\"menuName\":{\"en\":\"Zenduit Proximity\"},\"url\":\"https://apps.zenduit.com/proximity/proximity.html\"}],\"version\":\"1.0\",\"key\":\"12345-MYAPIKEY\",\"isSigned\":false}",
                    "{\"supportEmail\":\"support@zenduit.com\",\"signature\":\"1245-MYSIGNATURE\",\"name\":\"Zenduit Route Importer\",\"version\":\"1.0\",\"items\":[{\"path\":\"RoutesLink/\",\"menuName\":{\"fr\":\"Importer itinéraire\",\"en\":\"Import Route\"},\"url\":\"https://bitbucket.org/!api/2.0/snippets/zenduitteam/9ez58R/f14c245b7705c6fae15f6097137a98d973075015/files/index.html\"}],\"key\":\"12345-MYAPIKEY\",\"isSigned\":false}",
                    "{\"name\":\"Gofleet Support\",\"supportEmail\":\"support@gofleet.ca\",\"version\":\"1.0\",\"items\":[{\"url\":\"https://apps.zenduit.com/gofleet-support/iframe.html\",\"icon\":\"https://apps.zenduit.com/gofleet-support/gofleet.png\",\"path\":\" GofleetSupport\",\"menuName\":{\"en\":\"Add Support Ticket\"}}],\"isSigned\":false}",
                    "{\"name\":\"KPI\",\"supportEmail\":\"support@gofleet.ca\",\"version\":\"1.0\",\"items\":[{\"icon\":\"https://kpi.zenduit.com/Images/Icon.png\",\"url\":\"https://kpi.zenduit.com/kpiaddin.html\",\"path\":\"https://kpi.zenduit.com/kpiaddin.html\",\"menuName\":{\"en\":\"ZenScore\"}},{\"icon\":\"https://kpi.zenduit.com/Images/Icon.png\",\"url\":\"https://kpi.zenduit.com/kpiaddin.html\",\"path\":\"DriveAppLink/\",\"menuName\":{\"en\":\"ZenScore\"}}],\"isSigned\":false}",
                    "{\"name\":\"ZenduHeatmap\",\"supportEmail\":\"support@geotab.com\",\"version\":\"1.0\",\"items\":[{\"url\":\"https://apps.zenduit.com/ZenduHeatmap/heatmap.html\",\"path\":\"RuleAndGroupsLink/\",\"icon\":\"https://apps.zenduit.com/ZenduHeatmap/images/icon.png\",\"menuName\":{\"en\":\"Zendu Heatmap\"}}],\"isSigned\":false}",
                    "{\"supportEmail\":\"chanpark@gofleet.ca\",\"signature\":\"1245-MYSIGNATURE\",\"name\":\"Route Importer\",\"version\":\"1.0\",\"items\":[{\"path\":\"RoutesLink/\",\"menuName\":{\"fr\":\"Importer itin?raire\",\"en\":\"Import Route\"},\"url\":\"https://dl.dropboxusercontent.com/s/nk5whr5xxbz8b2c/index.html\"}],\"key\":\"12345-MYAPIKEY\",\"isSigned\":false}",
                    "{\"name\":\"DFB\",\"supportEmail\":\"shane@zenduit.com\",\"version\":\"1.0\",\"items\":[{\"url\":\"https://dnata-flightboard-test.firebaseapp.com/assets/dispatcher.html\",\"path\":\"\",\"icon\":\"https://dnata.zenduit.com/BoardAddIn/Images/Logo2.png\",\"menuName\":{\"en\":\"Flightboard\"}}],\"isSigned\":false}",
                    "{\"name\":\"Flightboard Map Button\",\"supportEmail\":\"support@gofleet.com\",\"Author\":\"Shane Graham\",\"version\":\"1.0\",\"items\":[{\"page\":\"map\",\"click\":\"https://apps.zenduit.com/flightboard.js\",\"buttonName\":{\"en\":\"Flight Board\"}}],\"isSigned\":false}"
                ],
                "customWebMapProviderList": [
                    {
                        "createLayersDictionary": "[{\"type\":\"osm\",\"name\":\"Openstreet\",\"urls\":[\"http://tile.openstreetmap.org/${z}/${x}/${y}.png\"],\"options\":{\"isBaseLayer\":true}},{\"type\":\"wms\",\"name\":\"test\",\"urls\":[\"https://dnata.zenduit.com/Geoserver/geoserver/cite/wms\"],\"params\":{\"FORMAT\":\"image/png\",\"VERSION\":\"1.1.1\",\"TRANSPARENT\":true,\"LAYERS\":\"cite:airport2\"},\"options\":{\"isBaseLayer\":false,\"maxZoom\":19}}]",
                        "createMapOptions": "{\"units\":\"m\",\"projection\":\"EPSG:3857\",\"numZoomLevels\":19}",
                        "id": "dnata_1_1561029468843",
                        "name": "DNATA-1"
                    }
                ],
                "dataVersion": "e660c453-428e-4f80-abf6-403fb236d201",
                "distanceIncreaseFactor": 0,
                "ftpFeederLastIdSettings": {
                    "lastGpsId": "0000000000000000",
                    "lastStatusDataId": "0000000000000000",
                    "lastTripId": "0000000000000000",
                    "latestRunningDate": "0001-01-01"
                },
                "fuelTransactionImportSettingsList": [],
                "fuelSettings": {
                    "transactionTripMatchingBuffer": 360,
                    "dateCompareHoursEngineToCard": 720,
                    "locationDistanceBuffer": 1000,
                    "maxThreshold": 20,
                    "minThreshold": 10,
                    "maxWindowSize": 5,
                    "minWindowSize": 1,
                    "minSnapStartThreshold": 5,
                    "looseTransactionMatching": false,
                    "fuzzyTransactionMatching": false,
                    "indeterminateDuration": "28.00:00:00",
                    "maxDegreeOfParallelism": 1,
                    "derivedVolumeOveragePercent": 10
                },
                "isBackup": false,
                "liveDataValidityInterval": "00:10:00",
                "mapDownloadUrlList": [],
                "mapProvider": "GoogleMaps",
                "maximumStopInterval": "10675199.02:48:05.4775807",
                "minimumStopInterval": "00:00:00",
                "passwordPolicy": {
                    "isLowerCaseRequired": false,
                    "isNonAlphanumericRequired": false,
                    "isNonUserNameRequired": true,
                    "isNumericRequired": false,
                    "isUpperCaseRequired": false,
                    "minimumLength": 8
                },
                "userLockoutPolicy": {
                    "failureAuthenticationsPerGracePeriod": 3,
                    "failureAuthenticationsGracePeriod": "00:15:00",
                    "lockoutUnitTime": "00:30:00"
                },
                "purgeSettings": {
                    "currentSchedule": 0,
                    "maintenanceSchedules": [
                        {
                            "enablePurge": false,
                            "lastPurgeDate": "2019-02-07T15:47:10.498Z",
                            "maintenancePeriod": "Day",
                            "purgePeriodToKeep": "Forever",
                            "hosPurgePeriodToKeep": "Forever",
                            "dvirPurgePeriodToKeep": "Forever",
                            "privatePurgePeriodToKeep": "Forever",
                            "sqlServerName": "mybackup",
                            "timeOfMaintenance": "00:10:46.0920000",
                            "allowedUserInactivityPeriod": 0,
                            "groupId": "GroupCompanyId"
                        }
                    ]
                },
                "percentOfLeasedVehicles": -1,
                "percentOfTelematicsDevices": -1,
                "lastCompanyProfileExportToBq": "2019-07-05T08:58:56.179Z",
                "lastUpdate": "2019-03-19T12:16:44.434Z",
                "directSupportAcceptedDate": "0001-01-01",
                "clearanceUpdater": "",
                "resellerInfo": {
                    "address": "Dubai United Arab Emirates",
                    "contactEmail": "supportuae@gofleet.com",
                    "name": "Go Fleet International",
                    "telephoneNumber": "+16478751070"
                },
                "smallCacheMode": true,
                "speedingGraceDuration": "00:00:16",
                "speedingTrigger": [
                    113,
                    129,
                    145
                ],
                "stopSpeedGo": 5,
                "zoneNeighbourhoodLimit": -1,
                "zoneNeighbourhoodLimitWhenMoving": -1,
                "dvirSettings": {
                    "vehiclesDefectTreeId": "b2711",
                    "trailersDefectTreeId": "b2759"
                },
                "tripUploadVersion": "0000000000786f27",
                "allowResellerAutoLogin": true,
                "databaseOwner": "bi@gofleet.com",
                "maxPCDistance": -1,
                "userTimeoutPeriod": 0,
                "allowedUserInactivityPeriod": 0,
                "storedPreviousPasswordHashes": 0,
                "passwordLifetime": "00:00:00"
            }
        ]
    ];
    const device = results[0][0],
        diagnosticEngineHoursAdjustmentId = results[1][0],
        diagnosticOdometerAdjustmentId = results[2][0];
    const odometerData = diagnosticOdometerAdjustmentId.data > 0
        ? (diagnosticOdometerAdjustmentId.data / 1000).toFixed(0)
        : diagnosticOdometerAdjustmentId.data;
    const engineHours = (sec => {
        const hours = Math.round(sec / 3600);
        const minutes = Math.round((sec - hours * 3600) / 60);

        return `${hours}hours ${minutes}minutes`;
    })(diagnosticEngineHoursAdjustmentId.data);

    //Fuel usage today in geotab???
    const fuelUsageToday = 44;
    const auxOneTotalHours = 44;

    elt.innerHTML = ` 
        <div class="addin"> 
            <div class="data-block">
                <button class="accordion">Asset: {device.name}</button> 
                <div class="panel"> 
                    <h6>Asset Icon: icon pick list</h6> 
                     
                    <h6>Measurements:</h6> 
                    <ul> 
                        <li>Odometer: {odometerData}km</li> 
                        <li>Engine Hourd: {engineHours(diagnosticEngineHoursAdjustmentId.data)}</li> 
                        <li>Fuel Usage Today: {fuelUsageToday}L</li> 
                        <li>Aux 1: {auxOneTotalHours}</li> 
                    </ul> 
                     
                    <h6>Faults:</h6> 
                    <ul> 
                        <li>Fault Name broken today: {number of time broken}</li> 
                    </ul> 
                     
                    <h6>Rules:</h6> 
                    <ul> 
                        <li>Rule name being broken: start time rule broken | rule hours broken for</li> 
                        <li>Rule name being broken: start time rule broken | rule hours broken for</li> 
                        <li>Rule name being broken: start time rule broken | rule hours broken for</li> 
                    </ul> 
                </div> 
                 
                <button class="accordion">Driver: {driver.name}</button> 
                <div class="panel"> 
                    <h6>Employee Details:</h6> 
                    <ul> 
                        <li>Group: {driver.group}</li> 
                        <li>Designation: {designation}</li> 
                        <li>Employee Number: {epmlNo}</li> 
                        <li>Employee Comments: {emplComments}</li> 
                        <li>Phone Number: {emplPhoneNo} {send sms}</li> 
                    </ul> 
                     
                    <h6>Weekly Safety Score:</h6> 
                    <ul> 
                        <li>Average Idling/Stop: {idlingTime}</li> 
                        <li>Weekly Idling Cost: {idlingCost}</li> 
                        <li>Speeding/100: {tally over 1 week}</li> 
                        <li>Aggressive Driving/100: {tally over 1 week}</li> 
                    </ul> 
                     
                    <h6>House of Service:</h6> 
                    <ul> 
                        <li>Current Availability Hours Left: {hours and link to availability report}}</li> 
                        <li>Violations Today: {number and link to geotab report}</li> 
                        <li>Violations This Week: {number and link to geotab report}</li> 
                    </ul> 
                </div> 
                 
                <button class="accordion">Zone: {zone.name}</button> 
                <div class="panel"> 
                    <h6>Customer Details:</h6> 
                    <ul> 
                        <li>Company Name: <input type="text"  placeholder="add company name"></li> 
                        <li>Street: <input type="text"  placeholder="add address"></li> 
                        <li>Suite: ?</li> 
                        <li>Province: {province}</li> 
                        <li>State: {state}</li> 
                        <li>Contact Name: {name}</li> 
                        <li>Phone: {phone}</li> 
                        <li>Mobile: {mobilePhone}</li> 
                        <li>Email: {email}</li> 
                    </ul> 
                     
                    <h6>Customer Comments:</h6> 
                    <div> 
                        <input type="text"  placeholder="add comments"> 
                    </div> 
                     
                    <h6>Recent Visits:</h6> 
                    <ul> 
                        <li>Jan 23/2019 3:30pm by {asset.name}</li> 
                        <li>Jan 23/2019 3:30pm by {asset.name}</li> 
                        <li>Jan 23/2019 3:30pm by {asset.name}</li> 
                        <li>Jan 23/2019 3:30pm by {asset.name}</li> 
                        <li>Jan 23/2019 3:30pm by {asset.name}</li> 
                    </ul>         
                </div>
            </div>
            <div class="tips-block">
                <span>Click the asset on the map for details</span>            
            </div> 
        </div>`;

    const acc = document.getElementsByClassName("accordion");
    const dataBlock = document.querySelector('.data-block');
    const tipsBlock = document.querySelector('.tips-block');
    dataBlock.style.display = 'none';

    for (let i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            const panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    };

    const getDataFromGeotab = (typeName, deviceID, searchDate, successCb, errorCb) => {
        service.api.multiCall([['Get', {
            "typeName":typeName,
            "search":{
                "id":deviceID
            }
        }],
            ['Get', {
                "typeName":"StatusData",
                "search":{
                    "deviceSearch":{
                        "id":deviceID
                    },
                    "diagnosticSearch":{
                        "id":"DiagnosticEngineHoursAdjustmentId"
                    },
                    "fromDate":searchDate,
                    "toDate":searchDate
                }
            }],
            ['Get', {
                "typeName":"StatusData",
                "search":{
                    "deviceSearch":{
                        "id":deviceID
                    },
                    "diagnosticSearch":{
                        "id":"DiagnosticOdometerAdjustmentId"
                    },
                    "fromDate":searchDate,
                    "toDate":searchDate
                }
            }],
            ['Get', {
                "typeName":"TripTypeChange",
                "search":{
                    "deviceSearch":{
                        "id":deviceID
                    },
                    "toDate":searchDate,
                    "fromDate":searchDate,
                    "includeFromDateOverlap":true
                }
            }],
            ['Get', {
                "typeName":"DriverChange",
                "search":{
                    "deviceSearch":{
                        "id":deviceID
                    },
                    "type":"Driver",
                    "fromDate":searchDate,
                    "toDate":searchDate,
                    "includeOverlappedChanges":true
                }
            }],
            ['Get', {
                "typeName":"WorkTime"
            }],
            ['GetTimeZones', {

            }],
            ['Get', {
                "typeName":"SystemSettings"
            }]])
            .then(successCb)
            .catch(errorCb);
    };

    const clickHandler = (data) => {
        const {entity: search, type: typeName} = data;
        if(typeName === 'device') {
            const searchDate = new Date().toISOString();
            const onSucces = results => {
                tipsBlock.style.display = 'none';
                dataBlock.style.display = 'block';
                console.log('<<<results array', results);
            };
            const onError = err => {
                console.log('<<<error', err);
            };
            getDataFromGeotab('Device', search.id, searchDate, onSucces, onError)
        }
    };
    service.events.attach('click', clickHandler);
}; 
